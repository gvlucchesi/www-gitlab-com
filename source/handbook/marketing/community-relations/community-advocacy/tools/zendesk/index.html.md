---
layout: markdown_page
title: "Community advocacy tools"
---

## On this page
{:.no_toc}

- TOC
{:toc}

----

## Zendesk instance

![Mentions in Zendesk](/images/handbook/marketing/community-relations/zendesk.jpg){: .shadow}

The Community Advocates team use a [dedicated Zendesk instance](https://gitlab-community.zendesk.com/) to centralize the management of mentions in the [social response channels](/handbook/marketing/community-relations/community-advocacy/#community-response-channels).

The majority of the response channels' mentions are routed to Zendesk via automation. Generally through Zapier or a native Zendesk integration.

Each mention is then converted into a ticket and filtered into a [View](#zendesk-views), ordered by channel priority. As part of their daily workflow, Community Advocates process each one of these views and manage the ticket lifecycle.

A typical ticket lifecycle is as follows:

1. A wider community member mentions GitLab in one of our [social response channels](/handbook/marketing/community-relations/community-advocacy/#community-response-channels)
1. The social response automation routes the mention to Zendesk and converts it into a ticket.
1. The ticket appears in one of the [View](#zendesk-views) as New
1. A Community Advocate scans through the views by priority or expertise, and opens the ticket
1. The Community Advocate decides the type of response. Typically, this will be either replying on the social channel, [involving experts] or marking the ticket as NOOP (no action required).
1. After the action for the ticket has been taken, the ticket will be closed by the Community Advocate, either manually or via a [macro](#zendesk-macros).

### Zendesk subscription

After [a discussion with the GitLab Support team](https://gitlab.com/gitlab-com/support/support-team-meta/issues/1583), it was concluded that for the specific Advocate workflows and metrics it would be best to keep the [Community Relations Zendesk instance]((https://gitlab-community.zendesk.com/)) separate from the [Support team's Zendesk instance](https://support.gitlab.com).

The Community Relations instance runs on a [Professional Zendesk Support](https://www.zendesk.com/product/pricing/#support_pricing) subscription, with one seat per Advocate plus an extra seat for the team manager.

### Zendesk access

Zendesk access is provided for every Community Advocate and their manager via the Zendesk admin. Once access is set up, Advocates can log in as "Agent" via SSO and their GitLab Google Suite account.

## Zendesk views

### View limits workaround

![Views in Zendesk](/images/handbook/marketing/community-relations/zendesk-views.jpg){: .shadow}

There is a [limit on the number of views](https://support.zendesk.com/hc/en-us/articles/231732128) that can be displayed on the main "Views" panel on Zendesk, regardless of the type of subscription:

- 12 shared views
- 8 personal views

If the number of views exceeds these limits, the additional views are moved to the `Settings > Views` panel. As the Community Advocates make use of the shared views feature, and as the number of response channels already exceeds the limit of 12, this poses a hindrance on ticket visibility and effectivity.

As a workaround, the team has decided to standardize the personal views for each Advocate, thus effectively increasing the number of visible Views to 20. If the number of response channels exceed the count of 20, an alternative approach could be to consolidate some of the existing views –a practice already followed where it makes sense (e.g. E-mail view).

The workaround is based on two steps: creating a shared view, which then every advocate can clone as a personal view.

To create a new shared view:

1. Navigate to `Settings > Views`
1. Click on the `Add view` button or clone an existing shared view
1. Set up or modify the filter conditions if necessary
1. On `Available for`, choose `All agents`
1. Inform the rest of the team about the availability of the new view  

To create a personal view from a shared view:

1. Navigate to `Settings > Views`
1. In the views list, hover over the right hand side of view you want to clone
1. Click on the ellipsis to open the context menu
1. Choose `Clone` 
1. Do not modify the filter conditions
1. On `Available for`, choose `Me only`  

## Zendesk macros

## Zendesk ticket lifecycle