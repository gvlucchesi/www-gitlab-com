---
layout: markdown_page
title: Setting ticket priority
category: Zendesk
---

### On this page
{:.no_toc}

- TOC
{:toc}

----

## Setting Ticket Priority

If a customer submits a ticket via the web ticket form, they can choose the starting priority of the ticket - this is the 'Customer Priority' field you will see in Zendesk. On ticket creation a trigger sets the main 'Priority' field to match the 'Customer Priority' choice.

If a customer emails in a ticket it will get a Priority of 'Normal' (unless it is sent to the special emergency contact).

Manually setting a ticket's priority in Zendesk will change the overall ticket [SLA](/handbook/support/support-engineering/working-with-tickets.html#sla), for both the first and next replies. This allows support to prioritize tickets and update the urgency during the life of the ticket (for example the initial request may be 'High' priority and then follow up questions may need 'Low' priority.)

### Guidelines and Effects

Please refer to the [guidelines and SLA listed in our statement of support](/support/#standard-support).
